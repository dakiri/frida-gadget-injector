#!/bin/bash

version=12.8.14

mkdir arm64-v8a
wget https://github.com/frida/frida/releases/download/${version}/frida-gadget-${version}-android-arm64.so.xz
mv frida-gadget-${version}-android-arm64.so.xz arm64-v8a/frida-gadget-${version}.so.xz

mkdir armeabi
wget https://github.com/frida/frida/releases/download/${version}/frida-gadget-${version}-android-arm.so.xz
mv frida-gadget-${version}-android-arm.so.xz armeabi/frida-gadget-${version}.so.xz

mkdir armeabi-v7a
wget https://github.com/frida/frida/releases/download/${version}/frida-gadget-${version}-android-arm.so.xz
mv frida-gadget-${version}-android-arm.so.xz armeabi-v7a/frida-gadget-${version}.so.xz

mkdir x86
wget https://github.com/frida/frida/releases/download/${version}/frida-gadget-${version}-android-x86.so.xz
mv frida-gadget-${version}-android-x86.so.xz x86/frida-gadget-${version}.so.xz

mkdir x86_64
wget https://github.com/frida/frida/releases/download/${version}/frida-gadget-${version}-android-x86_64.so.xz
mv frida-gadget-${version}-android-x86_64.so.xz x86_64/frida-gadget-${version}.so.xz
