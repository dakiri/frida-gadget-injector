#!/usr/bin/python3.6

import lief
import zipfile
import shutil
import subprocess
import tempfile
import os
import pathlib
import requests
import re
from shutil import copyfile
from xtract import xtract

def inject(libdir, arch, selected_library,version):
    filename = "frida-gadget-"+version+".so.xz"
    src="./gadgets/%s/%s"%(arch,filename)
    dst="./%s/%s/%s"%(libdir,arch,filename)
    copyfile(src, dst)
    xtract(str(libdir / arch / filename))
    gadget_name = "libgdgt.so"
    os.rename(libdir / arch / filename[:-3], libdir / arch / gadget_name)
    os.remove (libdir / arch / filename)
    print(f"[+] Injecting {gadget_name} into {arch}/{selected_library} \n")
    libcheck_path = libdir / arch / selected_library
    libcheck = lief.parse(libcheck_path.as_posix())
    libcheck.add_library(gadget_name)
    libcheck.write(libcheck_path.as_posix())



version="12.8.14"
print("Working with version %s of gadget\n"%version)
apk_src = input("[+] Enter the path of your APK: ")
apk_src_stripped=os.path.splitext(apk_src)[0]
apk_dest_unsigned=apk_src_stripped+"-unsigned.apk"
apk_dest=apk_src_stripped+"-patched.apk"

try:
    f = open(apk_src,'r+')
except:
    print("[+] I did not find the file at, "+str(apk_src))
    exit()

workingdir="./src"
print(f"[+] Decompile the {apk_src} in {workingdir}")
print("apktool d %s -o src"%workingdir)
os.system("apktool d %s -o %s"%(apk_src,workingdir))
libdir = pathlib.Path(workingdir) / "lib"

gadget_architecture = {
     "arm64" : "android-arm64.so",
     "arm64-v8a" : "android-arm64.so",
     "armeabi" : "android-arm.so",
     "armeabi-v7a" : "android-arm.so",
     "x86" : "android-x86.so",
     "x86_64" : "android-x86_64.so"
}

print("[+] Select the architecture of for the target: ")
print("If you don't know run: adb shell getprop ro.product.cpu.abi\n")

architectures = os.listdir(libdir)

counter_lib = 0
final_architectures = []
for i in architectures:
    if i in gadget_architecture:
        counter_lib+=1
        final_architectures.append(i)
        print(f"{counter_lib}) {i}")

print(f"{(counter_lib+1)}) Inject frida-gadget for all architectures [Default]\n")

selected_arch = input("> ")

if (selected_arch==""):
    selected_arch=counter_lib+1
    libraries = os.listdir(libdir/final_architectures[0])
else:
    if int(selected_arch) == (counter_lib+1):
        libraries = os.listdir(libdir/final_architectures[0])
    else:
        libraries = os.listdir(libdir/final_architectures[int(selected_arch)-1])

counter = 0
for lib in libraries:
    counter += 1
    print(str(counter) + ") "+lib)

if (counter>1):
    print("\n[+] In which library do you want to inject?")
    print("[+] Enter the number of the desired library: ")
    library = input("> ")
    selected_library = libraries[int(library)-1]
else:
    library=counter-1
    selected_library = libraries[0]

if int(selected_arch) == (counter_lib+1):
    for arch in final_architectures:
        inject(libdir, arch, libraries[int(library)-1],version)
else:
    inject(libdir,final_architectures[int(selected_arch)-1],libraries[int(library)-1],version)

print("[+] APK Building...")

os.system("apktool b src -o %s"%apk_dest_unsigned)

print("[+] APK Signing ...")

print("apksigner sign --ks ./store.jks --ksAlias business --ksKeyPass 'redteams.fr' --ksPass 'redteams.fr' -a %s"%apk_dest_unsigned)
os.system("apksigner sign --ks ./store.jks --ksAlias business --ksKeyPass 'redteams.fr' --ksPass 'redteams.fr' -a %s"%apk_dest_unsigned)
print(f"[+] SUCCESS!! \n")
