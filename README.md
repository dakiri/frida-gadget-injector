# Frida Gadget Lief Injector

Script pour automatiser l'injection de gaget frida dans un apk Android. 
Source d'inspiration [https://gitlab.com/jlajara/frida-gadget-lief-injector.git]

Les gadgets sont mis en cache dans ./gadget le script gadgets/grab.sh permet d'actualiser le cache (editer le numero de version souhaité dans le script).



## Requirements

```
Python 3.6

pip3 install lief 
```

## Usage

```
python3 patch
```

