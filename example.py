import frida, sys
def on_message(message, data):
    if message['type'] == 'send':
        print("[*] {0}".format(message['payload']))
    else:
        print(message)
jscode = """


Java.perform(function () {

  var Log = Java.use("android.util.Log");
  Log.v("frida-lief", "Have fun!");


Java.enumerateLoadedClasses({
		onMatch: function(classname){
			if (classname.indexOf("XmlPullParser")>-1){
			console.log(classname);
		}
		},

		onComplete: function (){
		}
	});
});
"""
process = frida.get_usb_device().attach('gadget')
script = process.create_script(jscode)
script.on('message', on_message)
print('[*] Running CTF')
script.load()
sys.stdin.read()
